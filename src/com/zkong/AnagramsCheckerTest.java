package com.zkong;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnagramsCheckerTest {
	
	
	
	@BeforeClass
    public static void oneTimeSetUp() {
        // one-time initialization code   
    	System.out.println("@BeforeClass - oneTimeSetUp");
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
    	System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Before
    public void setUp() {
    	System.out.println("@Before - setUp");
    }
    
    @After
    public void tearDown() {
       
        System.out.println("@After - tearDown");
    }

    @Test
    public void testNull() {
    	assertFalse( AnagramsChecker.isAnagram(null, null));
    	assertFalse( AnagramsChecker.isAnagram(null, ""));
		assertFalse( AnagramsChecker.isAnagram("", null));
		
    }

    @Test
    public void testEmpty() {
    	assertTrue( AnagramsChecker.isAnagram("", ""));
    }
	
    
	@Test
	public void AnagramsCheckertest() {
		String[] in = { "node", "salvador dali", "stream" };
    	String[] out = { "done", "avida dollars", "master" };
		
		for (int i = 0; i < in.length; i++){
			assertTrue( AnagramsChecker.isAnagram(in[i], out[i]));
		}
	}
}
