package com.zkong;


/*
 * Write a function to determine if two strings of unknown length (but not longer than 

100 MB each) are anagrams of each other, that is, contain exactly the same number 

of each letter of the alphabet. It should be case insensitive, should ignore all 

characters but the 26 letters of the English alphabet, should be very time-efficient, 

and should be space-efficient. Include a brief explanation of why and how it works 

efficiently. Include tests if you have time.
*/

public class AnagramsChecker {
	
	public static boolean isAnagram( String a, String b) {
		
		if( a == null || b == null)
			return false;
		
		//In ASCII table, a is 97, z is 122
		int[] result = new int[123]; //Since array starts with 0, need size to be 122+1 in order to put char Z
		
		//
		for(char c : a.toCharArray()) {
			result[Character.toLowerCase(c)]++; 
		}
		
		for(char c : b.toCharArray()) {
			result[Character.toLowerCase(c)]--; 
		}
		
		for(int i= 97; i<=122; i++) {
			if(result[i]>0)
				return false;
		}
		return true;	
	}
	
	public static void main(String[] args) {
		
		//Two input string of unknown length
		String unknowLengthString1="";
		String unknowLengthString2="";
		
		if( isAnagram(unknowLengthString1, unknowLengthString2) ){
			System.out.println("Two strings are anagrams of each other");
		} else {
			System.out.println("Two strings are NOT anagrams of each other");
		}	
    }

}
